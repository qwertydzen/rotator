<?php
/**
 * Description of Hit
 *
 */
class Hit
{
    private $_set = 'hits';
    private $_prefix = 'hits|';
    private $_instance = null;
    private $_resources = array();
    private $_redis_host = 'localhost';
    private $_redis_port = 6379;

    /**
     * Send http request for sending
     * logs to hits.twittweb.com
     *
     * @param int $type Log name
     * @param int $counter Value of counter
     * @return String|false
     */
    public function hit($type, $counter = 1) {
        $url = "http://my.hitsmetric.com/hits/send/$type/$counter";
        return file_get_contents($url);
    }

    /**
     * Name or value of logs:
     * @param string $log
     * @return int Current value
     */
    public function asyncHit($log) {
        $adapter = $this->getRedis();
        $r = $adapter->increment($this->getName($log));

        $adapter->addToSet($this->_set, $log);

        return $r;
    }

    /**
     * Compile names
     *
     * @param string $log
     * @return string
     */
    private function getName($log) {
        $name = $this->_prefix.$log;
        return $name;
    }

    public function instance() {
        if (empty($this->_instance)) {
            $this->_instance = new self();
        }

        return $this->_instance;
    }

    /**
     * @usage CLI
     * @return void
     */
    public function rotate() {
        $adapter  = $this->getRedis();
        $logs = $this->getLogs();

        foreach ($logs as $log) {
            $counter = $adapter->setAndGet(
                                    $this->getName($log), 0 // Get count value and reset it
                                );
            echo "Sending log($log) value: $counter...";
            if ($counter > 0){
            	$this->hit($log, $counter);
            }
            echo "Done." . PHP_EOL;
        }
    }

    /**
     * Return availible list of logs
     * @return array
     */
    private function getLogs() {
        return $this->getRedis()
                        ->getSet($this->_set);
    }

    private function getRedis(){
        $key = 'redis';
        if (empty($this->_resources[$key])) {
            $redis = array(
                    "servers" => array(
                        array(
                            "host" => $this->_redis_host,
                            "port" => $this->_redis_port,
                        )
                    )
            );
            require_once 'Rediska.php';
            $this->_resources[$key] = new Rediska($redis);
        }

        return $this->_resources[$key];
    }

    public function __construct($redis_host, $redis_port){
        $this->_redis_host = $redis_host;
        $this->_redis_port = $redis_port;
    }
}


require_once 'Cronfile.php';
$cronfile = new Cronfile(__FILE__);
$chp = $cronfile->CheckCountProcesses(1);
if(!$chp){
    die('Too many processes');
}

include('servers.php');

$hits = array();
$serv_hosts = array();
foreach($servers as $server){
    try{
        $hits[] = new Hit($server, 6379);
        $serv_hosts[] = $server;
    }catch(Exception_Hit $e){
        echo "$server init error".PHP_EOL;
    }
}

while(1){
    foreach($hits as $id=>$hit){
        echo 'Host ' .$serv_hosts[$id] . ' rotating hits'.PHP_EOL;
        $hit->rotate();
    }
}

?>
